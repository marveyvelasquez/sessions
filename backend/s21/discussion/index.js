console.log("Hi, B297!");


function printline() {
	console.log ("Frankly my dear, I don't give a damn!");
}

printline();
printline();
printline();
printline();
printline();


/*Syntax:
function functionName
*/

//Function Declaration
function printName (){
	console.log("My name is Marv");
}

//Function Invocation
printName();

declaredFunction();

// Function declaration vs expressions

//Function Declration
	//is created with the function keyword and adding a function name
	//they are "saved for later use"

function declaredFunction(){
	console.log("Hello from declaredFunction!");

};

declaredFunction();



//Function Expression
	//is stored in a variable
	//is an anonymous fucntion assigned tot he variable declaredFunction
	
	let variableFunction = function(){
		console.log ("Hello from function expression!");
	};

	variableFunction();

//a function expression of function named funcName assigned to the variable funcExpression

	let funcExpression = function funcName() {
		console.log ("Hello from the other side!");

	};

	funcExpression();


	//We can also reassign declared fucntyions and function expressions to new anonymous function
	declaredFunction = function(){
		console.log("updated declaredFunction");
	};
	declaredFunction();

	funcExpression = function (){
		console.log("Updated funcExpression");

	};

	funcExpression();

	const constantFunc = function(){
		console.log("Initialized with const");
	};

	constantFunc();


	//Function Scope

	/*
		Scope -accessibility?visibility of variables
		JS varaibles has 3 types of scope
		1. local/block scope
		2. global scope
		3. Function scope
	
	*/
/*
{
	let a = 1;
}

let a = 1;

function sample (){
	let a = 1;
};*/


{
	let localVar = "Armando Perez";
}

let globalVar = "Mr. Worldwide";

console.log(globalVar)

function showNames (){
	var functionVar = "Joe";
	const functionConst = "John";
	let functionLet = "Jane";
	console.log(functionVar);
	console.log(functionConst);
	console.log(functionLet);

};
showNames();


//Nested Functions

function myNewFunction (){
	let name = "Jane";

	function nestedFunction(){
		let nestedName = "John"
		console.log(nestedName);
	}
	//console.log(nestedName);
	nestedFunction();
}
myNewFunction();

let globalName = "Cardo";

function myNewFunction2(){
	let nameInside = "Hillary";
	console.log(globalName);
};
myNewFunction2();
//console.log(nameInside); //Uncaught ReferenceError: nameInside is not defined at inde


//Using ALert()
//alert("This will run immediately when the page loads.")

function showSampleAlert(){

	alert("Hello earthlings! This is from a function!");
}
showSampleAlert();
console.log ("I will only log in the console when the alert is dismissed")

//Using prompt()

let samplePrompt =prompt("Enter your name: ");
console.log("Hi, I am " + samplePrompt);


function printWelcomeMessage (){
	let firstName = prompt("Enter your first name: ");
	let lastName = prompt("Enter your last name: ");
	console.log("Hello, " + firstName + " " + lastName + "!");
	console.log("Welcome to my page!");
};
printWelcomeMessage();

//The Return Statement

function returnFullName(){
	return "Jeffrey" + ' ' + "Smith" + ' ' + "Bezos";
	console.log("This message will not be printed!");
}
let fullName = returnFullName();
console.log(fullName);


function returnFullAddress(){
	let fullAddress = {

		street: "#44 Maharlika Street",
		City: "Cainta",
		Province: "Rizal"
	};
	return fullAddress;
};
let myAddress = returnFullAddress();
console.log(myAddress);

function printPlayerInfo(){
	console.log ("Username: " + "dark_magician");
	console.log ("Level: " + 95);
	console.log("Job: " + "Mage");
}
let user1 = printPlayerInfo();
console.log(user1);//undefined

function returnSumOf5and10(){
	return 5+10;
}
	let sumOf5And10 = returnSumOf5and10;
	console.log(sumOf5And10);

	let total = 100 + returnSumOf5and10();
	console.log(total);