//Syntax, Statements, and Comments

console.log("We tell the computer to log this in the console.");
alert("We tell the computer to display an alert with this message!");

//Statement in programming are instructions that we tell the computer to perform
	//JS Statements usually ends with semicolon (;)
	//train us to locate where a statement ends
	
//Syntax in programming, it is the set of rules that describes how statements must be constructed
//All lines/blocks of code should be written in a specific manner to work.
//This is due to how these codes were initially programmed to function and perform in a certain manner	

// Comments
//For us to create a comment, we use Ctrl + /
// We have single line (ctrl+/) and miltiline comments (ctrl + shift + /)

// console.log("Hello");

/*alert("This is an alert!");
alert("This is another alert");*/

// Whitespaces (space and line breaks) can impact functionality  in many computer languages, but not in JS.


//Variables
	//it is used to contain data
	//any information that is used by our applications are storede in what we call a memory
	//when we create varaibles, certain portions of a device's memory is given a "name" that we call "variables"

	//this makes it easier for us to associate information stored in our dewices to actual "names" about information

		let myVariable;
		console.log(myVariable);

	//Declaring variables
		//tells our devices that a variable name is created and is ready to store data
		//declaring a variable without giving it a value will automatically assign it with the value of "undefined" meaning that the variable's value is "not defined"
		//variables must be declared first before they are used
		//using variables before they are declared will return an error

		let hello;
		console.log(hello);

		// declaring and initializing variables
		// syntax
			//let/const variableName = intialValue;

		let productName = 'desktop computer';
		console.log(productName);
		let productPrice = 18999;
		console.log(productPrice);

		const interest = 3.539;

		//re-assigning variable value

		productName = 'laptop';
		console.log(productName);

		//interest = 3.5;
		//console.log(interest);

		let friend = 'kate';
		friend = 'Jane';

		let supplier;

		supplier = "John Smith Tradings";
		supplier = "Zuitt Store";

		//we cannot declare a const variable without initiation
		/*const pi;
		pi = 3.1416
		console.log(pi);*/

		//Multiple variable declaration

		let productCode = 'DC017', productBrand = 'Dell';

		//let productCode = 'DC017';
		//const productBrand = "Dell";
		console.log(productCode, productBrand);

		//Data Typess

		//Strings
		//Strings are a series of characters that create a word, phrase, or a sentence or anything related to a creating text

		let country = 'Philippines';
		let province = 'Metro Manila';


		//Concat Strings
		let fullAddress = province + ', ' + country
		console.log(fullAddress)

		let greeting = 'I live in the ' + country;
		console.log(greeting)

		//the escape character (\) in strings in combination with other characters can produce different effects

		//"\n" referes to creating a new line  in betwee text

		let mailAddress = 'Metro Manila\n\nPhilippines';
		console.log(mailAddress);

		let message = "John's employees went home early";
		console.log(message)
		message = 'John\'s employees went home early';
		console.log(message)

		//Numbers

		let headcount=26;
		console.log(headcount);
		let grade = 98.7;
		console.log(grade);
		let planetDistance=2e10;
		console.log(planetDistance);

		//Combine text and strings
		console.log("John's first grade last quarter is " + grade);

		//Boolean
		let isMarried = false;
		let isGoodConduct = true;
		console.log(isMarried);

		console.log("isGoodConduct:" + isGoodConduct);


		//Arrays

			//let/const arrayName = [elementA, elementB, .....]

		let grades = [98.7, 92.1, 90.7, 98.6];
		console.log(grades);


		//Objects

		let myGrades = {
			firstGrading: 98.7,
			secondGrading: 92.1,
			thirdGrading: 90.7,
			fourthGrading: 98.6
		}

		console.log(myGrades);
		console.log(typeof myGrades)


		let person = {
			fullName: 'Juan dela Cruz',
			age: 35,
			isMarried: false,
			contact: ["+639123456789", "87000"],
			address: {
				houseNumber: '345',
				city: 'Manila'
			}

		}
		console.log(person)

		//typeof operator
		console.log(typeof person);


//Null
	//it is used to intentionally express the absence of a value in a variable declaration/initialization
	//null simply means that a data type was assigned to a variable but it does not hold any value/amount or is nullified

		let spouse = null;




//Undefined
	//represents the state of a variable that has been declared but without an assigned value

		let fullName;
		console.log(fullName);//undefined